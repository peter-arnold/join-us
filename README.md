# Hello World #

Give us your best shot.

Seriously.  We don't care how long you took on your code test.   We're not bean-counters.  

We are, however, a start-up.  And pragmatism and simplicity are what we favour more than anything else.

If you can prove it, with data, then I'll probably let you do it.  

Add your project in a folder that is your name/handle/email/watever.  Submit a pull request.  And accept the terms and conditions [x] that your code remains public.  Having a public repo of our code tests actually makes it harder for people to just copy stuff they found on stack overflow.   

